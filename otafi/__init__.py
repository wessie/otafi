from __future__ import unicode_literals
from otafi.util import import_class


class Result(dict):
    """
    A dictionary containing the result of otafi's processing. This is an ordinary
    dictionary with the one exception that it has an attribute named :attr:`bundle`
    that is the bundle used by otafi for this result.
    """
    def __init__(self, info, bundle=None):
        super(Result, self).__init__(info)
        self.bundle = bundle


def parse(tokenizer, data):
    tokenizer = aliases.get(tokenizer, tokenizer)

    if not callable(tokenizer):
        try:
            tokenizer = import_class(tokenizer)
        except ValueError:
            raise ValueError("Invalid tokenizer: '%s'" % tokenizer)
        except ImportError:
            mod, _ = tokenizer.rsplit('.', 1)
            raise ValueError("Module '%s' can't be imported" % mod)
        except AttributeError:
            mod, klass = tokenizer.rsplit('.', 1)
            raise ValueError("Class '%s' does not exist in '%s'" % klass, mod)

    bundle = tokenizer().parse(data)

    return bundle.to_dict()



# We keep a list of aliases for tokenizers that are bundled with otafi. This
# makes it easier for library users, since they won't have to remember the
# exact location of the tokenizer class.
aliases = {
    "anime": "otafi.contrib.anime.AnimeTokenizer",
    "manga": "otafi.contrib.manga.MangaTokenizer",
}