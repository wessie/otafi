"""
The following module contains all the base classes required to add to otafi
functionality.

The main force of otafi is an :class:`Analyzer`. This class receives a data
set it should analyze and sort by their respective meaning. For example an
analyzer for video information should be able to see '720p' as a resolution.

There is one special cased analyzer, we call this a :class:`Tokenizer`. The
difference between the two is that a :class:`Tokenizer` receives a string as
data. The tokenizer is responsible for the initial `tokenizing` of the string.
So if for example you had the string 'My favourite show - 01 [720p].mkv' the
tokenizer should return something similar to ['My favourite show', '01', '720p',
'mkv'].

Of course since the tokenizer is often specialised to their respective
content. You can make the :class:`Tokenizer` act as an :class:`Analyzer` as well
(in fact, the :class:`Tokenizer` inherits from :class:`Analyzer`). Take the
previous example, the tokenizer should know that 'My favourite show' is in
reality the name of the show. This means we can mark it as such directly by
assigning to the :attr:`OtafiResult.info` **name** key. This way there is no
extra analyzer required to know that this part is the name of the show.

You can do anything you can do in an :class:`Analyzer` in a :class:`Tokenizer`
as well. We only make a distinction between the two because of the data type
they receive. The simplest way of remembering this is to say that a
:class:`Tokenizer` is the *first analyzer* while any :class:`Analyzer` is anything
**but** the first analyzer.
"""
from __future__ import unicode_literals
from otafi import Result
from otafi.util import import_class
import logging
import os.path


logger = logging.getLogger("otafi")
__all__ = ["AnalyzerBase", "Tokenizer", "Bundle"]


class AnalyzerBase(object):
    """
    Class that is the base of all tokenizers, should be inherited and all methods
    marked for user implementing should be implemented.

    You require to supply the following methods in your subclass:

    - :meth:`process`

    These are optional:

    - :meth:`process_after`

    See their individual documentation for their use case.
    """
    logger = logging.getLogger("otafi.analyzers")
    next = None

    def initial_parse(self, data):
        bundle = Bundle(data=data)

        if os.path.isfile(data):
            bundle.info['filepath'] = data
            bundle.data = os.path.basename(data)

        return bundle

    def parse(self, data):
        """
        Entry point for parsing any kind of data. This should be called when
        using *otafi* classes and not :meth:`process` directly.
        """
        if not isinstance(data, Bundle):
            bundle = self.initial_parse(data)
        else:
            bundle = data

        bundle = self.process(bundle)

        if self.next is not None:
            bundle = self._handle_next(self.next, bundle)

        return self.process_after(bundle)

    def process(self, data):
        """
        Process :obj:`data` which is either an unicode string or a
        :class:`Bundle` instance. A tokenizer gets an unicode string, while
        all others get a :class:`Bundle` instance.


        :params data: A unicode string or :class:`Bundle` instance.

        :return: :class:`Bundle` instance.
        """
        raise NotImplementedError

    def process_after(self, data):
        """
        An optional after-processing, this is useful when you've set :attr:`next`
        and want to do something extra after the analyzer in :attr:`next` is
        done with it.
        """
        return data

    def debug(self, message, *args, **kwargs):
        """
        Logs a debug message, this can be used for debugging purpose by any
        of the analyzers. All messages are logged to :attr:`logger`. Which is
        a Logger from the standard libraries :mod:`logging` module.

        This prefixes the message with the class name before sending it to the
        logger. This means you don't have to worry about differentiating when
        calling this from your own class.
        """
        prefix = "%s: " % self.__class__.__name__

        return self.logger.debug(prefix + message, *args, **kwargs)

    def _handle_next(self, analyzer, bundle):
        """
        Calls the :meth:`parse` on the :obj:`analyzer` passed.

        If the input is a string, this tries to import it as a module.class
        pattern.

        If the input is a callable, it is used as is.
        """
        if not callable(analyzer):
            try:
                cls = import_class(analyzer)
            except ImportError:
                # TODO: Make this not ignore errors
                cls = None
        else:
            cls = analyzer

        return cls().parse(bundle) if cls else bundle

class Bundle(object):
    """
    This class is used to return results across the tokenizers, it saves a certain
    amount of context about the process before passing it to the other processors.

    You are most likely to use this class either as a return value in your
    written :meth:`otafi.AnalyzerBase.process` or as an argument to a different
    processor.

    :attr:`raw`:
        A list of data that hasn't been identified yet. This is full near the
        start of a analyzing cycle, and should be near empty at the end.

    :attr:`info`:
        A dictionary of values that already have enough context that they don't
        need to be forced into an analyzer. Each analyzer has access to this
        just the same as the main data. You should use this in your tokenizer if
        you are fairly sure whatever you tokenize is the absolute result.

    :attr:`tokenizer`:
        The tokenizer class that was used to create this result, if this isn't
        set by the user the class that returned the :class:`Bundle` is
        added by the :class:`Tokenizer` class instead.

    :attr:`tokenizer_instance`:
        The tokenizer instance used for creation of this result, same as above
        with :attr:`tokenizer`. This is set for you if untouched.

    """

    def __init__(self, data, raw=None, info=None):
        super(Bundle, self).__init__()

        self.info = info or {}

        self.raw = raw or []

        self.tokenizer = None
        self.tokenizer_instance = None

        self.data = data

    def __repr__(self):
        return "Bundle(raw=%r, info=%r)" % (self.raw, self.info)

    def to_dict(self):
        """
        Turns the bundle into a :class:`otafi.Result` instance.
        """
        return Result(self.info, bundle=self)

    def return_empty(self):
        """
        Makes the bundle return empty, this clears out any data found so far.

        Currently the clearing only clears up :attr:`info`.
        """
        self.info.clear()
        return self

