from __future__ import unicode_literals
from otafi.analyzer import AnalyzerBase, Bundle
import re
import os


# A regular expression that matches anything between [ and ] or ( and ).
# warning: This also matches on ( and ] or any other possible combination.
_re_enclosures = re.compile(r"[[(]([^\])]+?)[\])]")
_re_episode = re.compile(r"(?<=(?<=-)[_ .])(?<!Vol )(?<!PV )(?:E)?(?P<episode>[0-9]{1,3})(?!-[0-9]+)(?:v(?P<version>[0-9]+))?(?P<end>(?=[_ .\-\[]))?(?(end).|$)")


class AnimeTokenizer(AnalyzerBase):
    """
    A tokenizer for anime files, this assumes the filename to be in a format that
    has been common in the anime community for a while now. Anything not according
    to the common format has undefined results in this tokenizer.
    """
    next = "otafi.core.video.VideoAnalyzer"
    def process(self, bundle):
        data = bundle.data.replace("_", " ")


        # First check for any extensions if there are any.
        data, ext = os.path.splitext(data)
        if ext:
            self.debug("Adding %s as extension.", ext)
            # Cut off the period it starts with, don't need that.
            bundle.info['extension'] = ext[1:]

        self.debug("Starting enclosures with: %s", data)

        # Get all things in between [ ] and ( ).
        for match in _re_enclosures.finditer(data):
            match_data = match.group(1)

            self.debug("Appending %s as enclosure content.", match_data)

            bundle.raw.extend(match_data.split())


        # Remove all the blocks we just extracted by substituting
        data = _re_enclosures.sub('', data)

        self.debug("Starting episode with: %s", data)

        episode = _re_episode.search(data)

        if episode:
            episode_data = episode.groupdict()

            # We don't need this key inside the final result.
            del episode_data['end']

            self.debug("Updating result info: %r", episode_data)

            bundle.info.update(episode_data)

        # Remove the extracted pieces, and strip it off whitespace on both sides
        data = _re_episode.sub('', data).strip()

        # The extraction sub does not remove any hyphens (-) left at the end
        # so we remove them manually here.
        if data.endswith('-'):
            # Remove the extra hyphen and make sure we strip any whitespace we
            # create on the right side by doing that.
            data = data[:-1].rstrip()

        self.debug("Adding 'name' result info: %s", data)

        # And add all our leftovers as name, we are fairly sure it is the name.
        bundle.info['name'] = data

        return bundle

    def process_after(self, bundle):
        # Here we assume that whatever was first in the result list, and hasn't
        # been caught by any of the analyzers is the sub group. This is kinda
        # wonky. But it works!
        bundle.info['group'] = bundle.raw[0]

        # Remove the entry we just pulled from the data list. Slice assignment!
        bundle.raw[:] = bundle.raw[1:]

        return bundle