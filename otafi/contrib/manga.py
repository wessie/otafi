"""
Parses manga filenames.
"""
from otafi.analyzer import Bundle, AnalyzerBase
import re
import os


_re_enclosures = re.compile(r"[[(]([^\])]+?)[\])]")
_re_volume = re.compile(r"v(?P<volume>\d+)", re.I)
_re_chapter = re.compile(r"c(h)?(?P<chapter>\d+)", re.I)
# Current implementation does a reverse before searching for the page.
# Make sure to account for that when editing this regex.
_re_integer = re.compile(r"(?P<page>(\d+-)?\d+)p?")

CUT_OUT = '************************'


class MangaTokenizer(AnalyzerBase):
    def process(self, bundle):

        # Usage of _ is common, so we replace it with a space.
        data = bundle.data.replace('_', ' ')

        # Get the extension if there is one.
        data, ext = os.path.splitext(data)

        if ext:
            bundle.info['extension'] = ext[1:]

        # We are assuming enclosures are unneeded at this point
        data = _re_enclosures.sub(CUT_OUT, data)

        # Can we find a volume maybe, this is often not included!
        volume = _re_volume.search(data)

        if volume:
            volume_data = volume.groupdict()

            bundle.info.update(volume_data)

        data = _re_volume.sub(CUT_OUT, data)


        # See if we can find any chapter in there.
        chapter = _re_chapter.search(data)

        if chapter:
            chapter_data = chapter.groupdict()

            bundle.info.update(chapter_data)

        data = _re_chapter.sub(CUT_OUT, data)

        # For the page number we assume that the integer showing up last in
        # `data` is the page number. We find it by... reversing our `data` and
        # then doing a search on it
        data = data[::-1]

        page = _re_integer.search(data)

        if page:
            page_data = page.group('page')[::-1]

            bundle.info['page'] = page_data
        else:
            # We couldn't find a page number... doubtful the input is an actual
            # manga, lets cut our self short with an empty bundle.
            bundle.raw.append(data[::-1])

            return bundle.return_empty()

        # Make sure CUT_OUT is reversed here, since otherwise we might get
        # undefined behaviour later.
        data = _re_integer.sub(CUT_OUT[::-1], data)

        # reverse the data back into its original form
        data = data[::-1]

        # Now we need to clean up our leftover data.
        # First, we can assume that anything we removed from the front is
        # uninteresting.
        data = data.lstrip(CUT_OUT)

        # Now, we can take everything from the front till the first CUT_OUT as
        # our preliminary title right away.
        index = data.find(CUT_OUT)

        title, data = data[:index], data[index:]

        # Get rid of any whitespace that might've sneaked in.
        title = title.strip()

        if not title:
            # We couldn't find a title, this most likely means we either got
            # something totally not manga, or just a very stupid formatted one.
            return bundle.return_empty()

        bundle.info['name'] = title

        # And now we clean up our mess and keep it as extra raw data for future
        # uses. Often this is just small things related to the page at hand.
        bundle.raw.append(data.replace(CUT_OUT, '').strip())

        return bundle