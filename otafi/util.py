from __future__ import unicode_literals
import importlib

def import_class(class_path):
    """
    Tries to import :obj:`class_path`. :obj:`class_path` should be a string
    of an absolute module path and class name to import.

    >>> klass = import_class("otafi.contrib.anime.AnimeTokenizer")

    :raises: ValueError if class_path is invalid.
    :raises: ImportError if class_path can't be imported.
    :raises: AttributeError if class does not exist.

    :returns: A class object.
    """
    try:
        module, cls = class_path.rsplit('.', 1)
    except ValueError:
        raise ValueError("Invalid class path: %s" % class_path)

    mod = importlib.import_module(module)

    return getattr(mod, cls)