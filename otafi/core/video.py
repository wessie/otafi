"""
An analyzer that matches details related to video. This includes common things
like resolution, format, container, source, encoding, etc.
"""
from __future__ import unicode_literals
from otafi.analyzer import AnalyzerBase
import re

try:
    import enzyme
except ImportError:
    enzyme = None


def in_known(known, vector):
    """
    Checks if :obj:`vector` is in :obj:`known`. Used for creation of factory
    matching functions.
    """
    return vector in known

def in_known_lower(known, vector):
    """
    This behaves the same as :func:`in_known` but lowers :obj:`vector` first.
    """
    return in_known(known, vector.lower())


_crc_regex = re.compile(r'[0-9A-F]{8}')
def crc_check(_, vector):
    """
    This checks if :obj:`vector` matches the regular expression '[0-9A-F]{8}'
    which is often a good shot at the :obj:`vector` being a CRC value.
    """
    return _crc_regex.match(vector)


def regex_check(regex, vector, _regex_cache={}):
    matcher = _regex_cache.get(regex)
    if matcher is None:
        matcher = re.compile(regex, re.I)
        _regex_cache[regex] = matcher

    return matcher.match(vector)


def known_factory(key, known_data, func=in_known):
    def func_known(data, vector):
        result = func(known_data, vector)
        if result:
            data.info[key] = vector
        # Return the result so that we can check if something matched or not in
        # the analyzer class.
        return result
    return func_known


known_resolutions = r"(\d+p|\d+x\d+)"

known_10bit = {"10bit", "hi10p", "hi10"}
known_video_codecs = {"x264", "h264", "h265", "vp8", "vp9", "h.264", "mpeg2", "xvid"}
known_audio_codecs = {"aac", "flac", "dts-es", "mp3", "dts"}
known_sources = {"bd", "tv", "blu-ray", "bluray", "dvd"}
known_devices = {"ps3", "xbox", "ipad", "android", "iphone", "ipod"}


known_functions = [
    known_factory("resolution", known_resolutions, regex_check),
    known_factory("10bit", known_10bit, in_known_lower),
    known_factory("video-codec", known_video_codecs, in_known_lower),
    known_factory("audio-codec", known_audio_codecs, in_known_lower),
    known_factory("source", known_sources, in_known_lower),
    known_factory("device", known_devices),
    known_factory("crc", None, crc_check),
]


class EnzymeAnalyzer(AnalyzerBase):
    """
    Analyzer that tries to extract extra information from the file if the input
    was a filename.
    """
    def process(self, bundle):
        if not 'filepath' in bundle.info:
            return bundle

        try:
            result = enzyme.parse(bundle.info['filepath'])
        except (enzyme.ParseError, ValueError):
            pass
        else:
            bundle.info['enzyme'] = result
        return bundle


class VideoAnalyzer(AnalyzerBase):
    """
    Analyzer that knows of various video properties. The current implementation
    recognizes the following properties:

    - resolution
    - video codecs
    - audio codecs
    - source material
    - encoded for which device
    - crc

    Implementation notes:
        This uses the list :obj:`known_functions` together with the factory
        function :func:`known_factory` for processing.

    """
    if enzyme:
        next = EnzymeAnalyzer

    def process(self, bundle):
        # A list of things we don't know about.
        leftover = []

        for vector in bundle.raw:
            # Make sure we reset our state variable.
            found = False

            for known_func in known_functions:
                # Warning: Don't change the order of the `or`. This is intended.
                # Due to short-circuiting known_func won't be called if you
                # turn this around.
                found = known_func(bundle, vector) or found

            if not found:
                leftover.append(vector)

        # Make sure we set our raw value to the leftovers we have now
        bundle.raw[:] = leftover


        # We check here if `extension` is set already by anyone previously. If
        # it is we check if it's a container type we copy it.
        extension = bundle.info.get("extension", "").lower()
        if extension and extension in {"mp4", "mkv", "avi", "ogm"}:
            bundle.info['container'] = extension

        return bundle
