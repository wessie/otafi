import otafi

def test_anime_parser(parser_input):
    input, expected = parser_input
    result = otafi.parse('anime', input)
    assert result == expected