import otafi

def test_manga_parser(parser_input):
    input, expected = parser_input
    result = otafi.parse('manga', input)
    assert result == expected