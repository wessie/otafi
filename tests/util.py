import ast
import io

open = io.open

def read_test_file(filename):
    with open(filename, 'r', encoding='utf8') as f:
        input_, expected = None, None
        for line in f:
            if input_ is not None and expected is not None:
                yield input_, expected
                input_, expected = None, None

            # Ignore commented lines
            if line.lstrip().startswith("#"):
                continue
            # Ignore empty lines
            if not line.strip():
                continue

            if input_ is None:
                input_ = line.strip()
            elif expected is None:
                expected = ast.literal_eval(line.strip())

        if input_ is not None and expected is not None:
            yield input_, expected


def write_test_file(filename, iterator):
    with open(filename, 'w', encoding='utf8') as f:
        for input_, expected in iterator:
            f.write(input_)
            f.write('\n')
            f.write(expected)
            # Add two newlines for readability.
            f.write('\n\n')