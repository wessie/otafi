import pytest
import os
import glob
from .util import read_test_file


test_directory = os.path.dirname(os.path.abspath(__file__))
parser_test_directory = os.path.join(test_directory, "contrib/")


def pytest_addoption(parser):
    parser.addoption("--parser-file-directory", action="append",
                     default=[parser_test_directory],
                     help="Directory to search for parser test files.")


def pytest_generate_tests(metafunc):
    if not "parser_input" in metafunc.fixturenames:
        return

    parameters = []

    name = metafunc.function.__name__
    prefix = name.split('_')[1]

    files = []
    for directory in metafunc.config.option.parser_file_directory:
        globber = "{directory:s}/{prefix:s}_parser_tests*.txt".format(
            directory=directory,
            prefix=prefix,
        )
        files += glob.glob(globber)

    for filename in files:
        parameters.extend(read_test_file(filename))

    metafunc.parametrize("parser_input",
                         parameters)