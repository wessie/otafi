otafi Package
=============

:mod:`otafi` Package
--------------------

.. automodule:: otafi.__init__
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`analyzer` Module
----------------------

.. automodule:: otafi.analyzer
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`util` Module
------------------

.. automodule:: otafi.util
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    otafi.contrib
    otafi.core

