contrib Package
===============

:mod:`contrib` Package
----------------------

.. automodule:: otafi.contrib
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`anime` Module
-------------------

.. automodule:: otafi.contrib.anime
    :members:
    :undoc-members:
    :show-inheritance:

