core Package
============

:mod:`core` Package
-------------------

.. automodule:: otafi.core
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`video` Module
-------------------

.. automodule:: otafi.core.video
    :members:
    :undoc-members:
    :show-inheritance:

