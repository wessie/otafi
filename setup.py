from setuptools import setup


setup(
    version="0.1",
    name="otafi",
    author="Wessie",
    author_email="otafi@wessie.info",
    url="https://github.com/Wessie/otafi",
    packages=["otafi"],
)