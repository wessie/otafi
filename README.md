Otafi
=====

[![Build Status](https://travis-ci.org/Wessie/otafi.png)](https://travis-ci.org/Wessie/otafi)
[![Code Quality](https://codeq.io/github/Wessie/otafi/badges/master.png)](https://codeq.io/github/Wessie/otafi/branches/master)

A filename parsing/creation tool for japanese related content


Documentation
=============

Documentation can be found at [readthedocs](https://otafi.rtfd.org/).
